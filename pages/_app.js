import React from 'react';
import { Provider } from 'react-redux';
import App, { Container } from 'next/app';
import { addLocaleData, IntlProvider } from 'react-intl';
import withRedux from 'next-redux-wrapper';
import createState from 'store/createState';
import languagesAntd from 'languages/antd';
import languagesLocal from 'languages/local';
import { LocaleProvider } from 'antd';
import { Header, Footer } from 'components/shared';
import '../static/styles/application.scss';

/**
 * @param {object} initialState
 * @param {boolean} options.isServer indicates whether it is a server side or client side
 * @param {Request} options.req NodeJS Request object (not set when client applies initialState from server)
 * @param {Request} options.res NodeJS Request object (not set when client applies initialState from server)
 * @param {boolean} options.debug User-defined debug mode param
 * @param {string} options.storeKey This key will be used to preserve store in global namespace for safe HMR
 */

class MyApp extends App {
  static async getInitialProps({ Component, ctx }) {
    // we can dispatch from here too
 

    // console.log(ctx);
    const pageProps = Component.getInitialProps ? await Component.getInitialProps(ctx) : {};

    return { pageProps, lang: 'ru' };
  }

  render() {
    const { Component, pageProps, store, lang } = this.props;

    return (
      <Container>
        <LocaleProvider locale={languagesAntd[lang][0]}>
          <IntlProvider locale={languagesAntd[lang][1]} messages={languagesLocal[lang]}>
            <Provider store={store}>
              {/* <Header /> */}
              <Component {...pageProps} />
              {/* <Footer /> */}
            </Provider>
          </IntlProvider>
        </LocaleProvider>
      </Container>
    );
  }
}

export default withRedux(createState)(MyApp);
