import React from 'react';
import Link from 'next/link';

function Home() {
  return (
    <div>
      <Link href="/"></Link> about
    </div>
  );
}

export default Home;
