import React from 'react';
import Error from 'next/error';
import PropTypes from 'prop-types';
import fetch from 'isomorphic-unfetch';

class Page extends React.Component {
  static async getInitialProps() {
    const res = await fetch('https://api.github.com/repos/zeit/next.js');
    const errorCode = res.statusCode > 200 ? res.statusCode : false;
    const json = await res.json();

    return { errorCode, stars: json.stargazers_count };
  }

  static propTypes = {
    errorCode: PropTypes.bool,
    stars: PropTypes.number,
  };

  static defaultProps = {
    errorCode: false,
    stars: 0,
  };

  render() {
    const { errorCode, stars } = this.props;
    if (errorCode) {
      return <Error statusCode={errorCode} />;
    }

    return <div>Next stars: {stars}</div>;
  }
}

export default Page;
