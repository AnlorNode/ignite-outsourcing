import React from 'react';
import s from './BackgroundVideo.scss';

class BackgroundVideo extends React.Component {
  render() {
    return (
      <div className={s.root}>
        <video src="../static/videos/Car.mp4" loop /* autoPlay */ muted className={s.video}></video>
        <div className={s.text}>
          <span className={s.spanWeb}>Web</span>Prototype
          <span className={s.spanStudio}>Studio</span>
        </div>
      </div>
    );
  }
}

export default BackgroundVideo;
