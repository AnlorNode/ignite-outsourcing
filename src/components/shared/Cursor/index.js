import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { injectIntl, FormattedMessage } from 'react-intl';
import { bindActionCreators } from 'redux';
import { Button } from 'antd';
import { seoRequest } from 'actions/globals';
import s from './Cursor.scss';

class Cursor extends React.Component {
  static propTypes = {
    action: PropTypes.PropTypes.shape({
      seoRequest: PropTypes.func.isRequired,
    }).isRequired,
    globals: PropTypes.PropTypes.shape({
      seoText: PropTypes.number.isRequired,
    }).isRequired,
  };

  handleChange = () => {
    const {
      action,
      globals: { seoText },
    } = this.props;
    action.seoRequest(seoText + 1);
  };

  render() {
    const {
      globals: { seoText },
    } = this.props;
    return (
      <div>
        <p className={s.text}>{seoText}</p>
        <Button onClick={this.handleChange} type="primary">
          <FormattedMessage id="press" />
        </Button>
      </div>
    );
  }
}

export default connect(
  ({ globals }) => ({ globals }),
  dispatch => ({
    action: bindActionCreators(
      {
        seoRequest,
      },
      dispatch,
    ),
  }),
)(injectIntl(Cursor));
