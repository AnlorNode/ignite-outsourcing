import cursor from './Cursor';
import header from './Header';
import footer from './Footer';
import BaackgroundVideo from './BackgroundVideo';

export const Cursor = cursor;
export const Header = header;
export const Footer = footer;
export const BackgroundVideo = BaackgroundVideo;
