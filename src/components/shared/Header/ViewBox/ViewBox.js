import React from 'react';
import { injectIntl, FormattedMessage } from 'react-intl';
import { Button, Modal, Row, Col } from 'antd';
import s from './ViewBox.scss';

class Header extends React.Component {
  state = {
    // Filtered stuff
    status: false,
  };

  render() {
    const { status } = this.state;
    return (
      <div>
        {status && (
          <React.Fragment>
            <Modal
              title=""
              visible={status}
              footer={null}
              // onOk={this.handleOk}
              onCancel={() => this.setState({ status: false })}
            >
              <Row type="flex" justify="center" className={s.cross}>
                <FormattedMessage id="main" />
              </Row>
              <Row type="flex" justify="center" className={s.cross}>
                <FormattedMessage id="portfolio" />
              </Row>
              <Row type="flex" justify="center" className={s.cross}>
                <FormattedMessage id="prices" />
              </Row>
              <Row type="flex" justify="center" className={s.cross}>
                <FormattedMessage id="about" />
              </Row>
            </Modal>
          </React.Fragment>
        )}
        {!status && (
          <Row
            type="flex"
            justify="end"
            className={s.svg}
            onClick={() => this.setState({ status: true })}
          >
            <svg
              width="20"
              height="20"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
              data-svg="navbar-toggle-icon"
            >
              <rect y="9" width="20" height="2"></rect>
              <rect y="3" width="20" height="2"></rect>
              <rect y="15" width="20" height="2"></rect>
            </svg>
          </Row>
        )}
      </div>
    );
  }
}

export default injectIntl(Header);
