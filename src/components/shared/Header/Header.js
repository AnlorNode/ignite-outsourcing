import React from 'react';
import { injectIntl, FormattedMessage } from 'react-intl';
import Link from 'next/link';
import { Button, Row, Col } from 'antd';
import ViewBox from './ViewBox';
import s from './Header.scss';

class Header extends React.Component {
  render() {
    return (
      <div className={s.root}>
        <dix className={s.menuNone}>
          <Row type="flex" justify="end" className={s.menu}>
            <Col md={{ span: 5 }}>
              <Link href="/">
                <div>
                  <a>inkies.digital</a>
                </div>
              </Link>
            </Col>
            <Col md={{ span: 3 }}>
              <Link href="/">
                <a>
                  <FormattedMessage id="main" />
                </a>
              </Link>
            </Col>
            <Col md={{ span: 3 }}>
              <Link href="/portfolio">
                <a>
                  <FormattedMessage id="portfolio" />
                </a>
              </Link>
            </Col>
            <Col md={{ span: 3 }}>
              <Link href="/prices">
                <a>
                  <FormattedMessage id="prices" />
                </a>
              </Link>
            </Col>
            <Col md={{ span: 3 }}>
              <Link href="/prices">
                <a>
                  <FormattedMessage id="about" />
                </a>
              </Link>
            </Col>
            <Col md={{ span: 5 }}>
              <Link href="/RequestСall">
                <Button className={s.custom}>
                  <FormattedMessage id="RequestСall" />
                </Button>
              </Link>
            </Col>
          </Row>
        </dix>
        <div className={s.viewBox}>
          <ViewBox />
        </div>
      </div>
    );
  }
}

export default injectIntl(Header);
