import ruRU from 'antd/lib/locale-provider/ru_RU';
import ukUA from 'antd/lib/locale-provider/uk_UA';
import enUS from 'antd/lib/locale-provider/en_US';

export default {
  ru: [ruRU, 'en-001'],
  uk: [ukUA, 'en-150'],
  en: [enUS, 'en'],
};
