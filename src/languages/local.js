import ru from './ru_RU';
import uk from './uk_UA';
import en from './en_US';

export default {
  ru,
  uk,
  en,
};
