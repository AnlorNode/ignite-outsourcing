import reducers from 'reducers/shared';
import thunk from 'redux-thunk';
import createFetch from 'fetch/createFetch';
import { createStore, applyMiddleware, compose } from 'redux';

export default (initialState, { isServer }) => {
  if (isServer) {
    return createStore(reducers, initialState);
  }
  return createStore(
    reducers,
    initialState,
    compose(
      x => x,
      applyMiddleware(
        thunk.withExtraArgument({
          fetch: a => {
            console.log(a, 'fetch');
          },
        }),
      ),
    ),
  );
};
