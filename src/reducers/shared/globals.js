import { handleAction } from 'redux-actions';
import { combineReducers } from 'redux';

const initialState = {
  seoText: 0,
};
const seoText = handleAction('globals', (state, action) => action.payload, initialState.seoText);
export default combineReducers({
  seoText,
});
