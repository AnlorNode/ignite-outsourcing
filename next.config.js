const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const withCSS = require('@zeit/next-css');
const withSASS = require('@zeit/next-sass');

module.exports = withSASS({
  webpack(config, { dev, isServer }) {
    config.module.rules.push({
      test: /\.less$/,
      use: [
        {
          // for HMR support.
          loader: !isServer && dev ? 'style-loader' : MiniCssExtractPlugin.loader,
        },
        { loader: 'css-loader' },
        {
          loader: 'less-loader',
          options: {
            javascriptEnabled: true,
          },
        },
      ],
    });


    return config;
  },
  cssModules: true,
  cssLoaderOptions: {
    importLoaders: 1,
    localIdentName: '[name]-[local]-[hash:base64:5]',
  },
  distDir: 'build',
});
